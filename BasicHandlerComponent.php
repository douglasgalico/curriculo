<?php

App::uses('AuthComponent', 'Controller/Component');


class BasicHandlerComponent extends Component
{


    public $components = array('Session','AdminOptions');

    protected $controller = null;

    public function __construct()
    {

        $this->User = ClassRegistry::init('Admin.User');
        $this->Representante = ClassRegistry::init('Representante');
        $this->Information = ClassRegistry::init('Information');
        $this->Warning = ClassRegistry::init('Warning');
        $this->Marca = ClassRegistry::init('Marca');
        $this->Categoria = ClassRegistry::init('Categoria');
        $this->Pedido = ClassRegistry::init('Pedido');

    }

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }


    public function startup(Controller $controller = null)
    {
        $this->controller = $controller;

    }


    public function Bootstrap()
    {
       // $this->AdminOptions->setOptionVal('teste','testaaaa');
        $id = AuthComponent::user('id');

        if ($this->checkRole(1)) {
            $this->_ownerBootstrap($id);
        } elseif ($this->checkRole(2)) {
            $this->_ownerBootstrap($id);
        } elseif ($this->checkRole(3)) {
            $this->_operBootstrap($id);
        } elseif ($this->checkRole(4)) {
            $this->_repreBootstrap($id);
        } elseif ($this->checkRole(5)) {
            //$this->controller->set('role','visit');
        }
    }


    public function getRepID(){
        $id = AuthComponent::user('id');
        if ($this->checkRole(4)) {
            $rep = $this->Representante->findByUserId($id);
            return $rep['Representante']['id'];
        }
        return null;
    }



    public  function checkRole($roleid = null){
        $role = AuthComponent::user('group_id');
        if ($roleid == $role){return true;}
        return false;
    }

    private function _ownerBootstrap($id)
    {
        $this->_userLoadandSet($id);
        $this->controller->set('l_informations',$this->Information->allRecentFromUser(AuthComponent::user('id'),true));
        $this->controller->set('l_informations_unread',$this->Information->countUnreadUsr(AuthComponent::user('id')));
        $this->controller->set('l_warnings',$this->Warning->allRecentFromUser(AuthComponent::user('id'),true));
        $this->controller->set('l_warnings_unread',$this->Warning->countUnreadUsr(AuthComponent::user('id')));

    }

    private function _operBootstrap($id)
    {
       // $this->controller->set('l_informations',$this->Information->allRecentFromUser(AuthComponent::user('id'),true));
       // $this->controller->set('l_warnings',$this->Information->allRecentFromUser(AuthComponent::user('id'),true));
        $this->_userLoadandSet($id);
    }

    private function _repreBootstrap($id)
    {


        $this->_userLoadandSet($id);
        $rep = $this->Representante->findById($this->controller->RepID);


        $this->controller->set('l_informations',$this->Information->allRecentFromUser(AuthComponent::user('id')));
        $this->controller->set('l_informations_unread',$this->Information->countUnreadUsr(AuthComponent::user('id')));
        $this->controller->set('l_warnings',$this->Warning->allRecentFromUser(AuthComponent::user('id')));
        $this->controller->set('l_warnings_unread',$this->Warning->countUnreadUsr(AuthComponent::user('id')));
        $this->controller->set('repinfo',$rep);
        $this->controller->set('repinfo_pedidoativo',$this->Pedido->findActivebyId($rep['Representante']['id']));
        $this->controller->set('l_marcas',$this->Marca->find('all',array('recursive' => -1,'fields' => array('Marca.name','Marca.id'))));
        $this->controller->set('l_categorias',$this->Categoria->find('all',array('recursive' => -1,'fields' => array('Categoria.name','Categoria.id'))));
    }

    private function _userLoadandSet($id = null)
    {

        if ($id) {
            $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => $id
                ), 'recursive' => -1
            ));
            $this->controller->set('userinfo',$user);
        }
    }

}