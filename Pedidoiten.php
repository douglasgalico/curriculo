<?php
App::uses('AppModel', 'Model');
/**
 * Pedidoiten Model
 *
 * @property Produto $Produto
 * @property Pedido $Pedido
 */
class Pedidoiten extends AppModel {



    public $Produto,$Pedido;
	/*public $belongsTo = array(
		'Pedido' => array(
			'className' => 'Pedido',
			'foreignKey' => 'pedido_id',
            'dependent' => true
		)
	);*/
    function beforeSave($options = array())
    {
        $this->Produto = ClassRegistry::init('Produto');
        $this->Pedido = ClassRegistry::init('Pedido');
        $pval = $this->Produto->getProductValuebyID($this->data[$this->alias]['produto_id']);
        parent::beforeSave();

        if(!$this->id && !isset($this->data[$this->alias][$this->primaryKey])) {
            $this->data[$this->alias]['querier'] = $this->Produto->getProductQuerierbyID($this->data[$this->alias]['produto_id']);
            $this->data[$this->alias]['unitario'] = $pval;
        }
        if(isset($this->data[$this->alias]['readd'])){
            if($this->data[$this->alias]['readd'] == 1){
                $this->Pedido->id = $this->data[$this->alias]['pedido_id'];

                if(($this->Pedido->field('valortotal') + $pval) > ($this->Pedido->getRepLim() + 30.00)){
                    return false;
                }
                if($this->Pedido->field('pedidostatus_id') != 1){
                    return false;
                }
            }
        }



    }

    public function afterSave($created, $options = array()) {
        $this->Produto = ClassRegistry::init('Produto');
        $this->Pedido = ClassRegistry::init('Pedido');
        if ($created){
            $this->Pedido->somaItem($this->Produto->getProductValuebyID($this->data[$this->alias]['produto_id']));
            $this->Produto->id = $this->data[$this->alias]['produto_id'];
            $this->Produto->stockOp('sub');
            return true;
        }else{
            return true;
        }
    }

    public function beforeDelete($cascade = true) {
        $this->Produto = ClassRegistry::init('Produto');
        $this->Produto->id = $this->field('produto_id');
        $this->Produto->stockOp('sum');

        return true;
    }
}
