<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 01/04/2015
 * Time: 16:18
 */



App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('Controller', 'Controller');
App::uses('MailgunComponent', 'Mailgun.Controller/Component');

class MailgunControllerTest extends Controller {
    public $paginate = null;
}

class MailgunComponentTest extends CakeTestCase {
    public $Mailgun = null;
    public $Controller = null;

    public function setUp() {
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->Mailgun = new MailgunComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new MailgunControllerTest($CakeRequest, $CakeResponse);
        $this->Mailgun->startup($this->Controller);
    }

    public function testFactory(){
        $domain = "teste.com.br";
        $unit = $this->Mailgun->instance('key-43e8bfdndsg4s63af0e7e66269');
        $result = $unit->sendMessage($domain, array(
            'from'    => 'Teste<no-reply@teste.com.br>',
            'to'      => 'Baz <contato@teste.com.br>',
            'subject' => 'Teste',
            'text'    => 'Testing some Mailgun awesomness!'
        ));
        echo print_r($result);
      //  $this->assertEquals('a',$unit->);


    }





    public function tearDown() {
        parent::tearDown();
        // Clean up after we're done
        unset($this->Mailgun);
        unset($this->Controller);
    }

}