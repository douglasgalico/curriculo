Instalação:

Inicializar o plugin no bootstrap:

CakePlugin::load(array(
    'Mailgun' => array(
        'bootstrap' => true
    )
));

Uso da classe MailGun:

Inserir compopnente no Controller 'Mailgun.Mailgun' e instanciar novo objeto:

$mail = $this->Mailgun->instance($key);
$mail->sendMessage(...);


Uso do Tracker e dos Webhooks:

Escolher UMA action de um controller para todos os webhooks e inserir:

if($this->request->is('post')){
 if($this->Tracker->newTracked($this->request->data)){
           return true;
        }
}

Eventos do Tracker:

O Model Tracker dispara 2 eventos (beforesave e aftersave) ligados ao evento de origem Mailgun(ex. opened, clicked,...),
pode ser atribuido um listener na App principal conforme:

//boostrap.php
...
App::uses('MailtrackerListener', 'Events');
$track = ClassRegistry::init('Mailgun.Tracker');
$track->getEventManager()->attach(new MailtrackerListener());


//Events/MailtrackerListener.php

App::uses('CakeEventListener', 'Event');

class MailtrackerListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Mailgun.Model.Tracker.opened.beforeSave' => 'openedBefore',
            'Mailgun.Model.Tracker.opened.afterSave' => 'openedAfter'
        );
    }
    public function openedBefore(CakeEvent $event){
        Debugger::log("before");

    }
    public function openedAfter(CakeEvent $event){
        Debugger::log("after");

    }
}

