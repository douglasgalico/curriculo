<?php
App::uses('MailgunAppModel', 'Mailgun.Model');


/*
 * Tracker - Objeto de requisição POST dos webhooks da API do Mailgun
 *
 */

class Tracker extends MailgunAppModel
{


    private $_LastMsg = null;

    /*
     *  newTracked
     *  $data = array padrão API Mailgun
     *
     *  ->Eventos gerados:
     *
     *  label: Mailgun.Model.Tracker.{evento}.beforeSave - antes de salvar no bando de dados
     *  label: Mailgun.Model.Tracker.{evento}.beforeSave - após de salvar no bando de dados
     *  ex:
     *  label: Mailgun.Model.Tracker.opened.beforeSave
     */
    public function newTracked(array $data = null)
    {
        $this->_LastMsg = '';
        //Reforçar
        if (!isset($data['domain']) && !isset($data['event'])
            && !isset($data['timestamp']) && !isset($data['signature'])
            && !isset($data['signature'])
        ) {
            return false;
        }

        $Event = new CakeEvent(
            'Mailgun.Model.Tracker.'.$data['event'].'.beforeSave',
            $this,
            array(
                'data' => $data
            )
        );
        $this->getEventManager()->dispatch($Event);


        $savedata = array();
        $this->create();

        foreach (array_keys($data) as $key) {
            $savedata[$this->alias][$key] = $data[$key];
        }

        if($this->save($savedata)){
            $Event = new CakeEvent(
                'Mailgun.Model.Tracker.'.$data['event'].'.afterSave',
                $this,
                array(
                    'data' => $data
                )
            );
            $this->getEventManager()->dispatch($Event);
            return true;
        }
        return false;


    }

}

?>