<?php


App::uses('Component', 'Controller');
App::uses('Controller', 'Controller');
App::uses('ComponentCollection', 'Controller');


require_once App::pluginPath('Mailgun') . 'vendor' . DS . 'autoload.php';
use Mailgun\Mailgun;

class MailgunComponent extends Component {

    protected  $Mailgun = null;




	protected $controller = null;

	public function initialize(Controller $controller) {

		$this->controller = $controller;
	}

	public function startup(Controller $controller = null) {
		$this->controller = $controller;
	}

    public function instance($key, $v = "v3"){
        return new Mailgun($key,"api.mailgun.net","v3");
    }


}
