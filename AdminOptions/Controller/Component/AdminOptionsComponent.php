<?php
/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 01/11/2014
 * Time: 00:49
 */
App::uses('Component', 'Controller');

class AdminOptionsComponent extends Component {

    /*
     *
      foreach($this->uses as $model) {
$this->$model = ClassRegistry::init($model);
}
     */
    protected $controller = null;
    public function __construct(){

            $this->AdminOptions = ClassRegistry::init('AdminOptions.AdminOptions');
    }

    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }

    /**
     * startup - Fired after the controller's beforeFilter method.
     *
     * @return void
     **/
    public function startup(Controller $controller = null) {
        $this->controller = $controller;

    }


    public function upateConfigurations(){
        $result = $this->AdminOptions->find('all');
        foreach ($result as $res) {
            $key = $res['AdminOptions']['key'];
            $val = $res['AdminOptions']['value'];
            Configure::write($key, $val);
        }
    }
    public function setOptionVal($key,$value){
        $result = $this->AdminOptions->find('first', array(
            'conditions' => array(
                'AdminOptions.key' => $key
            )
        ));
        $id = $result['AdminOptions']['id'];
        $this->AdminOptions->id = $id;
        $this->AdminOptions->saveField('value', $value);
    }


} 