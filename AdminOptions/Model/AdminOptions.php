<?php

/**
 * Created by PhpStorm.
 * User: Douglas
 * Date: 29/10/2014
 * Time: 23:44
 */

App::uses('AdminOptionsModel', 'AdminOptions.Model');

class AdminOptions extends AdminOptionsModel
{

    public $name = 'AdminOptions';
    public $useTable = 'admoptions';


}