<?php echo $this->Html->css('magnific/magnific-popup'); ?>
<?php echo $this->Html->script('magnific/magnific',array('block' => 'scriptBottom')); ?>


<h3 class="page-title">Catálogo por categoria: <strong><?php echo $catalog_info_categoria['Categoria']['name']?></strong> <?php echo $catalog_info_categoria['Categoria']['desc']?></small></h3>


<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="">Catálogo</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="">Por categoria</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href=""><?php echo $catalog_info_categoria['Categoria']['name']; ?></a>
        </li>
    </ul>
</div>


<div class="popup-repcategoria">

    <div class="row">
    <?php foreach($catalog_produtos as $cprod):?>
        <div class="col-md-3 col-sm-4 col-xs-6 thumbprodutoscatalogo">
            <a href="http://d17ixnyn2lal3i.cloudfront.net/produtos/<?php echo $cprod['Produto']['imgfile']; ?>" title="<?php echo $cprod['Produto']['querier']; ?>">
                <img src="http://d17ixnyn2lal3i.cloudfront.net/produtos/<?php echo $cprod['Produto']['imgfile']; ?>" width="100%">
            </a>
            <small class="text-center"><strong><?php echo $cprod['Produto']['querier']; ?></strong><span style="color: #990000;font-weight: bold"> <?php echo ($cprod['Produto']['emstock'] ? '' : '(Fora de estoque!) ')?></span></small>
        </div>
    <?php endforeach?>
    </div>
</div>


<?php

$sc =  "$(document).ready(function() {
        $('.popup-repcategoria').magnificPopup({
        delegate:'a',
        type:'image',
        gallery:{enabled:true}
        });
    });";

   $this->Js->buffer($sc);
?>


